#-------------------------------------------------------------------------------
# Name:        module2
# Purpose:
#
# Author:      Anshit's PC
#
# Created:     09-06-2015
# Copyright:   (c) Anshit's PC 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import cv2
import cv2.cv as cv
import numpy as np
#img=cv2.imread("C:\Users\Anshit's PC\Desktop\eye.jpg",0)

cap=cv2.VideoCapture(0)
while(True):
    ret, img = cap.read(0)
    gray2 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

    gray = clahe.apply(gray2)
    gray = cv2.medianBlur(gray,5)
    y,x=gray.shape

    kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
    gray= cv2.erode(gray,kernel,iterations = 1)
    gray= cv2.dilate(gray,kernel,iterations = 1)
    edges = cv2.Canny(gray,100,200,3)
    edges=edges[200:350,250:450]
    circles = cv2.HoughCircles(edges,cv.CV_HOUGH_GRADIENT,1,20,param1=40,param2=20,minRadius=0,maxRadius=30)
    if circles is  not None:
        circles = np.uint16(np.around(circles))

        for i in circles[0,:]:
              #if i[0]>x/3.0 and i[1]>y/3.0 and i[2]>20 and i[2]<28:
              # draw the outer circle
                  cv2.circle(img,(250+i[0],200+i[1]),i[2],(255,0,0),2)
                  #draw the center of the circle
                  cv2.circle(img,(250+i[0],200+i[1]),2,(0,255,0),3)


    cv2.imshow('Gray',img)
    cv2.imshow('Edges',edges)
    if cv2.waitKey(10)==27 or cv2.waitKey(10)==ord('q'):
        break
cap.release()
cv2.destroyAllWindows()