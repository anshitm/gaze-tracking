#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Anshit's PC
#
# Created:     11-05-2015
# Copyright:   (c) Anshit's PC 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import cv2
from cv2 import *
import numpy as np
face_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_frontalface_alt.xml")
eye_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_eye.xml")
cap=cv2.VideoCapture(0)
while(True):
    ret, img = cap.read(0)
    gray2 = cvtColor(img, COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))   # agruements include the no. of blocks image is divided(title grid size) and hist eq applied on them individually
                                                                    # cliplimit higher clip limit higher contrast
    gray = clahe.apply(gray2)
    #gray = np.array(gray, dtype='uint8')
    faces = face_cascade.detectMultiScale(gray, 1.3 , 5)
    #print faces

    for (x,y,w,h) in faces:
        cv2.rectangle(img, (x,y), ((x+w),(y+h)), (255,0,0), 2)
        cv2.rectangle(gray, (x,y), ((x+w), (y+h)), (0, 255, 0), 4)
        roi_gray = gray[y:y+h/2, x:x+w/2]
        roi_color = img[y:y+h/2, x:x+w/2]
        eyes = eye_cascade.detectMultiScale(roi_gray,1.1, 4)
        for (ex,ey, ew, eh) in eyes:
            #cv2.rectangle(img, (x+ex,y+ey), ((x+ex+ew), (y+ey+eh)),(0,255,0), 3)
            #print ex,ew,ey,eh
            eye2=roi_color[ey:ey+eh,ex:ex+ew]
            eye1 = roi_gray[ey:ey+eh,ex:ex+ew]
            eye_dl = roi_gray[ey+eh/3.0:ey+2.0*eh/3.0 , ex:ex+ew/4]     #left part roi
            eye_dr = roi_gray[ey+eh/3.0:ey+2.0*eh/3.0 , ex+8*ew/10:ex+ew]  #right part roi
            #eye_d=clahe.apply(eye_d)
            edges = cv2.Canny(eye_dl,200,200,3)
            corners = cv2.goodFeaturesToTrack(eye_dl,1,0.04,30)
            if corners is None:
                #print"error"
                break
            #print"rcorners",len(corners)
            #print corners
            corners = np.int0(corners)

            for i in corners:
                a,b = i.ravel()
                cv2.circle(eye2,(a,b+eh/3),3,(0,255,0),-1)
            rcorners = cv2.goodFeaturesToTrack(eye_dr,1,0.04,30)
            if rcorners is None:
                #print"error"
                break
            rcorners = np.int0(rcorners)
            for i in rcorners:
                ar,br = i.ravel()
                cv2.circle(eye2,(8*ew/10+ar,br+eh/3),3,(0,255,0),-1)

            cv2.imshow('asnh',eye2)
            cv2.imshow('asnh2',edges)
    cv2.imshow('faces', img)
    cv2.imshow('ddd',gray)
    if cv2.waitKey(10) == ord('q'):
            break

cap.release()
cv2.destroyAllWindows()