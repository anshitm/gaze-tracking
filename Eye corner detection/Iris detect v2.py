#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Anshit's PC
#
# Created:     10-06-2015
# Copyright:   (c) Anshit's PC 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import cv2
from cv2 import *
import numpy as np
face_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_frontalface_alt.xml")
eye_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_eye.xml")
#eye_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_mcs_eyepair_big.xml")
#roi_gray=np.zeros((480,640))
cap=cv2.VideoCapture(0)
while(True):
    ret, img = cap.read(0)
    #img2=cv2.resize(img,(640,480))
    gray2 = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))   # agruements include the no. of blocks image is divided(title grid size) and hist eq applied on them individually
                                                                   # cliplimit higher clip limit higher contrast
    gray = clahe.apply(gray2)
    #gray = np.array(gray, dtype='uint8')
    kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
    faces = face_cascade.detectMultiScale(gray, 1.3 , 5)
    #print faces

    for (x,y,w,h) in faces:
        cv2.rectangle(img, (x,y), ((x+w),(y+h)), (255,0,0), 2)
        cv2.rectangle(gray, (x,y), ((x+w), (y+h)), (0, 255, 0), 4)
        roi_gray = gray[y:y+h/2, x:x+w/2]
        roi_color = img[y:y+h/2, x:x+w/2]
        eyes = eye_cascade.detectMultiScale(roi_gray,1.2, 4)
        for (ex,ey, ew, eh) in eyes:
            #cv2.rectangle(img, (x+ex,y+ey), ((x+ex+ew), (y+ey+eh)),(0,255,0), 3)
            #print ex,ew,ey,eh
            eye = roi_gray[ey:ey+eh,ex:ex+ew]
            eye2 = roi_color[ey:ey+eh,ex:ex+ew]
            circles = cv2.HoughCircles(eye,cv.CV_HOUGH_GRADIENT,1,40,param1=50,param2=20,minRadius=10,maxRadius=20)
            if circles is  not None:
                circles = np.uint16(np.around(circles))

                for i in circles[0,:]:
              #if i[0]>x/3.0 and i[1]>y/3.0 and i[2]>20 and i[2]<28:
              # draw the outer circle
                  print i[2]
                  cv2.circle(eye2,(i[0],i[1]),i[2],(255,0,0),2)
                  #draw the center of the circled
                  cv2.circle(eye2,(i[0],i[1]),2,(0,255,0),3)


    cv2.imshow('Gray',img)
    #cv2.imshow('Edges',eye2)
    if cv2.waitKey(10)==27 or cv2.waitKey(10)==ord('q'):
            break
cap.release()
cv2.destroyAllWindows()