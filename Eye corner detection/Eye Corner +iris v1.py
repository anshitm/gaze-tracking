#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Anshit's PC
#
# Created:     14-08-2015
# Copyright:   (c) Anshit's PC 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import cv2
import numpy as np
from cv2 import *

def eyedetect(roi_color,roi_gray):
        eye_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_eye.xml")  #loading harr classifier for eye
        kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))                                   #defining kernel for dilation
        roi_gray= cv2.dilate(roi_gray,kernel,iterations = 1)
        eyes_2 = eye_cascade.detectMultiScale(roi_gray,1.1, 5)
        for (ex,ey, ew, eh) in eyes_2:
            #cv2.rectangle(img, (x+ex,y+ey), ((x+ex+ew), (y+ey+eh)),(0,255,0), 3)
            #print ex,ew,ey,eh
            eye3=roi_color[ey:ey+eh,ex:ex+ew]
            eye4 = roi_gray[ey:ey+eh,ex:ex+ew]
            eye_dl2 = roi_gray[ey+eh/4:ey+2*eh/3 , ex:ex+ew/4]     #left part roi
            eye_dr2 = roi_gray[ey+eh/4:ey+2*eh/3 , ex+8*ew/10:ex+ew]  #right part roi
            #eye_d=clahe.apply(eye_d)

            circles = cv2.HoughCircles(eye4,cv.CV_HOUGH_GRADIENT,1,40,param1=50,param2=20,minRadius=10,maxRadius=20)
            if circles is  not None:
                circles = np.uint16(np.around(circles))

                for i in circles[0,:]:
              #if i[0]>x/3.0 and i[1]>y/3.0 and i[2]>20 and i[2]<28:
              # draw the outer circle
                  #print i[2]
                  cv2.circle(eye3,(i[0],i[1]),i[2],(255,0,0),2)
                  #draw the center of the circle
                  cv2.circle(eye3,(i[0],i[1]),2,(0,0,255),3)
            edges = cv2.Canny(eye_dl2,200,200,3)
            corners_l2 = cv2.goodFeaturesToTrack(eye_dl2,1,0.04,30)
            if corners_l2 is None:
                #print"error"
                break
            #print"rcorners",len(corners)
            #print corners
            corners_l2 = np.int0(corners_l2)

            for i in corners_l2:
                a,b = i.ravel()
                cv2.circle(eye3,(a,b+eh/4),3,(0,255,0),-1)

            #print pre_x,pre_y
            rcorners_2 = cv2.goodFeaturesToTrack(eye_dr2,1,0.04,30)
            if rcorners_2 is None:
                print"error"
                break
            rcorners_2 = np.int0(rcorners_2)
            for i in rcorners_2:
                ar,br = i.ravel()
                cv2.circle(eye3,(8*ew/10+ar,br+eh/4),3,(0,255,0),-1)

def main():
    face_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_frontalface_alt.xml")   # loading the harr-classifier for face
    #eye_cascade = CascadeClassifier("C:\OpenCV\sources\data\haarcascades\haarcascade_mcs_eyepair_big.xml")
    cap=cv2.VideoCapture(0)

    while(True):
        ret, img = cap.read(0)
        #img2=cv2.resize(img,(640,480))
        gray2 = cv2.cvtColor(img, COLOR_BGR2GRAY)
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))   # agruements include the no. of blocks image is divided(title grid size) and hist eq applied on them individually
                                                                        # cliplimit higher clip limit higher contrast
        gray = clahe.apply(gray2)
        faces = face_cascade.detectMultiScale(gray, 1.2 , 5)
        #print faces

        for (x,y,w,h) in faces:
            cv2.rectangle(img, (x,y), ((x+w),(y+h)), (255,0,0), 2)
            cv2.rectangle(gray, (x,y), ((x+w), (y+h)), (0, 255, 0), 4)
            roi_gray_left = gray[y:y+h/2, x:x+w/2]
            roi_color_left = img[y:y+h/2, x:x+w/2]
            roi_color_right=img[y:y+h/2,x+w/2:x+w]
            roi_gray_right=gray[y:y+h/2,x+w/2:x+w]
            eyedetect(roi_color_left,roi_gray_left)
            #eyedetect(roi_color_right,roi_gray_right,count,count2)
        cv2.imshow('faces', img)
        #cv2.imshow('ddd',gray)
        if cv2.waitKey(10) == ord('q'):
                break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()
