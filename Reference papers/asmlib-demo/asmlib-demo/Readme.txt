0. This is a demo compiled by VC 2010 express, with OpenCV 2.3.1
1. The model is only trained in frontal view, limited lighting variations.
2. You can try the three "bat" files directly in File Explorer:
     view_model.bat    View the statistical model.
     fit_cam.bat       Find your face landmarks via your webcam!
     fit_image.bat     Run it, drag an image file to the console window, 
                       and press enter to check results.
3. demo.exe is a command line tool, check the Wiki or run it without arguments
   to see what option it accepts.
4. If there are problems, please report issues in Google Code directly:
     http://code.google.com/p/asmlib-opencv/

CHEN Xing ( http://chenxing.name/ )